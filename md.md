# Markdown cheat sheet

This cheatsheet is about [CommonMark](https://commonmark.org) specification of Markdown. For original Markdown specification check [John Gruber's full spec of markdown](http://daringfireball.net/projects/markdown/).

**Table of contents:**

- [Headings](#headings)
- [Emphasis](#emphasis)
- [Paragraphs](#paragraphs)
- [Blockquotes](#blockquotes)
- [Lists](#lists)
- [Links](#links)
- [Images](#images)
- [Code](#code)

## Headings

```
# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6
```
or

```
Header 1
========
Header 2
--------
```

# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

## Emphasis

Syntax | Result
-------|-------
`**bold**` or `__bold__` | **bold**
`*italic` or `_italic_` | *italic*
`**Super**seed` | **Super**seed
`*Multi*tasking` | *Multi*tasking

## Paragraphs

Continuous lines of text with one or more blank lines between them.

For a line break add two spaces `  ` or a backslash `\` to the end of a line.

```
This is the first paragraph.

This is the second paragraph.

This is a\
linebreak.
```

This is the first paragraph.

This is the second paragraph.

This is a\
linebreak.

## Blockquotes

```
> Start line with greater `>` sign to blockquote.
>
> Keep quote together, blank lines inside blockquote must contain `>` sign.
> > Blockquotes can be nested too.
```

> Start line with greater `>` sign to blockquote.
>
> Keep quote together, blank lines inside blockquote must contain `>` sign.
> > Blockquotes can be nested too.

## Lists

```
* Undordered list starts each line with `*`, `+` or `-`
	* Nested list by indent
		1. Ordered list
		2. Start each line with _number_ followed by period `.` or right parenthesis `)`

* You can also use task syntax
	1. [x] buy milk
	2. [ ] learn python
	3. [ ] clean house

11\. Don't want a list? Just use a backslash to escape.
```

* Undordered list starts each line with `*`, `+` or `-`
 	* Nested list by indent
 		1. Ordered list
 		2. Start each line with _number_ followed by period `.` or right parenthesis `)`
* You can also use task syntax
	1. [x] buy milk
 	2. [ ] learn python
 	3. [ ] clean house

11\. Don't want a list? Just use a backslash to escape.

## Links

Link text is enclosed by square brackets `[]` and for inline links, link URL is enclosed by parenthesis `()`.

```
Turn url into link <https://duckduckgo.com>

[DuckDuckGo](https://duckduckgo.com) #relative url can be used /a.html

[DuckDuckGo][link1]
⋮
[link1]: https://duckduckgo.com "title" #title is optional
```

Turn url into link <https://duckduckgo.com>

[DuckDuckGo](https://duckduckgo.com)

[DuckDuckGo][link1]

[link1]: https://duckduckgo.com "title"

## Images

Images are almost same as links only difference is that image starts with explanation mark `!`.

```
![alt](elephant.png) #full url may be used - https://commonmark.org/help/images/favicon.png

![alt][id]
⋮
[id]: lion.png "title"
```

![Elephant](elephant.png)

![Lion][id]

[id]: lion.png "title"

## Code

To create inline code, enclose text with backticks `` ` ``. To create code block, indent each line with 4 spaces or place 3 backticks ```` ``` ```` on a line before and after the code block.

````
This is an `inline` code block.

    4 spaces indent code block
    another 4 spaces code block line

```
or use 3 backsticks
```
````

This is an `inline` code block.

    4 spaces indent code block
    another 4 spaces code block line

```
or use 3 backsticks
```
